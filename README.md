## Bitcoin Core

# Supported tags and respective `Dockerfile` links
- [latest](https://hub.docker.com/r/krzko/bitcoind/), [alpine-latest](https://hub.docker.com/r/krzko/bitcoind/), [0.15.0.1](https://hub.docker.com/r/krzko/bitcoind/)

# What Is A Full Node?

A full node is a program that fully validates transactions and blocks. Almost all full nodes also help the network by accepting transactions and blocks from other full nodes, validating those transactions and blocks, and then relaying them to further full nodes.

Most full nodes also serve lightweight clients by allowing them to transmit their transactions to the network and by notifying them when a transaction affects their wallet. If not enough nodes perform this function, clients won’t be able to connect through the peer-to-peer network—they’ll have to use centralized services instead.

Many people and organizations volunteer to run full nodes using spare computing and bandwidth resources—but more volunteers are needed to allow Bitcoin to continue to grow. This document describes how you can help and what helping will cost you.

[Reference](https://bitcoin.org/en/full-node)

# Quick Start

**Run container with named volume to persist your data.**
`$ docker container run -d -p 8333:8333 --name bitcoin -v bitcoin-data:/var/lib/bitcoin krzko/bitcoind`

**View the location of the bitcoin-data volume**
`$ docker volume inspect bitcoin-data`

If you wish to pre-create the volume prior to running your container, `docker volume create` is your friend.

**Run container, with the current directory used as the bind mount. Ensure you have the correct local file system permissions.**
`$ docker container run -d -p 8333:8333 --name bitcoin -v $(pwd):/var/lib/bitcoin krzko/bitcoind`

# Loggging

`$ docker container logs -f bitcoin`

# Let me in

`$ docker container exec -it bitcoin sh`

# What's inside?

The bare bones;
- Alpine Linux
- bitcoind
- bitcoin-cli
- bitcoin-tx